#!/bin/sh

icon_path="/usr/share/codium/resources/app/resources/linux"

if [ -f "$icon_path/code.png" ]; then
    mv "$icon_path/code.png" "$icon_path/code_backup.png"
    cp ./vscodium.png "$icon_path/code.png"
    echo "ok"
fi