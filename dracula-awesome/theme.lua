---------------------------
-- Nordic awesome theme --
---------------------------

-- created by gurpinder

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local awful = require("awful")
-- Widget and layout library
local wibox = require("wibox")

-- lain 
local lain = require("lain")


--- theme config

-- fonts
font_family                 =   "Jetbrains mono"
font_size                   =   10
main_font                   =   font_family .. " " .. font_size
icon_font_family            =   "Font awesome 5 free"
icon_font_size              =   12
icon_font                   =   icon_font_family .. " " .. icon_font_size

-- color scheme
primary_bg_color            =   "#282a36"
primary_fg_color            =   "#aaaaaa"
active_bg_color             =   "#bd93f9"
active_fg_color             =   "#3b4252" 
inactive_bg_color           =   primary_bg_color
inactive_fg_color           =   primary_fg_color
minimize_bg_color           =   primary_bg_color
minimize_fg_color           =   "#44475a"
urgent_bg_color             =   "#ff5555"
urgent_fg_color             =   "#f8f8f2"
occupied_bg_color           =   primary_bg_color
occupied_fb_color           =   active_bg_color
active_border_color         =   active_bg_color
inactive_border_color       =   inactive_bg_color
---


-- widgets color scheme
primary_widget_bg_color                 =   primary_bg_color
primary_widget_fg_color                 =   primary_fg_color

clock_widget_bg_color                   =   primary_widget_bg_color
clock_widget_fg_color                   =   "#50fa7b"
clock_widget_icon_bg_color              =   primary_widget_bg_color
clock_widget_icon_fg_color              =   "#50fa7b"

calendar_widget_bg_color                =   primary_widget_bg_color
calendar_widget_fg_color                =   "#ff79c6"
calendar_widget_icon_bg_color           =   primary_widget_bg_color
calendar_widget_icon_fg_color           =   "#ff79c6"

cpu_widget_bg_color                     =   primary_widget_bg_color
cpu_widget_fg_color                     =   "#ffb86c"
cpu_widget_icon_bg_color                =   primary_widget_bg_color
cpu_widget_icon_fg_color                =   "#ffb86c"

memory_widget_bg_color                  =   primary_widget_bg_color
memory_widget_fg_color                  =   "#bd93f9"
memory_widget_icon_bg_color             =   primary_widget_bg_color
memory_widget_icon_fg_color             =   "#bd93f9"

fs_widget_bg_color                      =   primary_widget_bg_color
fs_widget_fg_color                      =   "#8be9fd"
fs_widget_icon_bg_color                 =   primary_widget_bg_color
fs_widget_icon_fg_color                 =   "#8be9fd"

---


local theme = {}

-- fonts
theme.font              =   main_font
theme.icon_font         =   icon_font

-- bar
theme.bg_normal         =   primary_bg_color
theme.fg_normal         =   primary_fg_color
theme.bg_focus          =   active_bg_color
theme.fg_focus          =   active_fg_color
theme.bg_unfocused      =   inactive_bg_color
theme.fg_unfocused      =   inactive_fb_color
theme.bg_urgent         =   urgent_bg_color
theme.fg_urgent         =   urgent_fg_color
theme.bg_minimize       =   minimize_bg_color
theme.fg_minimize       =   minimize_fg_color
theme.bg_systray        =   primary_bg_color


-- tag list
theme.taglist_bg_focus 	        =   active_bg_color
theme.taglist_fg_focus 	        =   active_fg_color
theme.taglist_bg_urgent         =   urgent_bg_color
theme.taglist_fg_urgent 	    =   urgent_fg_color
theme.taglist_bg_occupied       =   occupied_bg_color
theme.taglist_fg_occupied       =   occupied_fb_color
theme.taglist_bg_empty          =   "transparent"
theme.taglist_fg_empty          =   minimize_fg_color
theme.taglist_font              =   font_family .. " " .. 10

-- task list
theme.tasklist_bg_focus             =   active_bg_color
theme.tasklist_fg_focus             =   active_fg_color
theme.tasklist_bg_normal            =   inactive_bg_color
theme.tasklist_fg_normal            =   inactive_fg_color
theme.tasklist_bg_minimize          =   minimize_bg_color
theme.tasklist_fg_minimize          =   minimize_fg_color
theme.tasklist_icon_size            =   64

-- window
theme.useless_gap           =   dpi(4)
theme.border_width          =   dpi(2)
theme.border_normal         =   inactive_border_color
theme.border_focus          =   active_border_color

-- menu
submenu_arrow_types = {
    '\u{f101}',
    '\u{f105}',
    '\u{f061}'
}
theme.menu_submenu_icon         =   nil
theme.menu_height               =   dpi(26)
theme.menu_width                =   dpi(150)
theme.menu_bg_focus 	        =   theme.taglist_bg_focus
theme.menu_bg_normal            =   theme.bg_normal
theme.menu_submenu              =   submenu_arrow_types[1] .. " "
theme.menu_border_color         =   active_border_color
theme.menu_border_width         =   dpi(1)

-- notification
theme.notification_border_width         =  	dpi(1)
theme.notification_border_color         = 	active_border_color
theme.notification_max_width            =   350
theme.notification_height               =   100
theme.notification_max_height           =   150
theme.notification_icon_size            =   72
theme.notification_bg                   =   primary_bg_color
theme.notification_fg                   =   primary_fg_color
theme.notification_margin               =   dpi(6)
theme.notification_opacity              =   1

-- wallpaper
theme.wallpaper         =   themes_path.."guru/linux.png"


--- widgets


-- CPU WIDGET
cpu_widget_icon =  wibox.widget {
    markup = ' \u{f2db} ',
    widget = wibox.widget.textbox
}
cpu_widget_icon.font = theme.icon_font
local cpu = lain.widget.cpu {
    settings = function()
        widget:set_markup(cpu_now.usage .. "% ")
    end
}
cpu.font = main_font
theme.cpu_widget = wibox.widget {
    {
        cpu_widget_icon,
        fg = cpu_widget_icon_fg_color,
        bg = cpu_widget_icon_bg_color,
        widget = wibox.container.background
    },
    {
        cpu,
        fg = cpu_widget_fg_color,
        bg = cpu_widget_bg_color,
        widget = wibox.container.background
    },
    widget = wibox.layout.fixed.horizontal
}

-- FS WIDGET
fs_widget_icon =  wibox.widget {
    markup = ' \u{f0a0}} ',
    widget = wibox.widget.textbox
}
fs_widget_icon.font = theme.icon_font
theme.fs_widget = wibox.widget {
    {
        fs_widget_icon,
        fg = cpu_widget_icon_fg_color,
        bg = cpu_widget_icon_bg_color,
        widget = wibox.container.background
    },
    -- {
    --     awful.widget.watch('bash /home/gurpinder/fs', 5)
    -- },
    widget = wibox.layout.fixed.horizontal
}


-- MEMORY WIDGET
memory_widget_icon =  wibox.widget {
    markup = ' \u{f538} ',
    widget = wibox.widget.textbox
}
memory_widget_icon.font = theme.icon_font
local memory = lain.widget.mem {
    settings = function()
        widget:set_markup(mem_now.used .. "M ")
    end
}
memory.font = main_font
theme.memory_widget = wibox.widget {
    {
        memory_widget_icon,
        fg = memory_widget_icon_fg_color,
        bg = memory_widget_icon_bg_color,
        widget = wibox.container.background
    },
    {
        memory,
        fg = memory_widget_fg_color,
        bg = memory_widget_bg_color,
        widget = wibox.container.background
    },
    widget = wibox.layout.fixed.horizontal
}

-- CALENDAR WIDGET
calendar_widget_icon = wibox.widget {
    markup = ' \u{f073} ',
    widget = wibox.widget.textbox
}
calendar_widget_icon.font = theme.icon_font
theme.calendar_widget = wibox.widget {
    {
        calendar_widget_icon,
        fg = calendar_widget_icon_fg_color,
        bg = calendar_widget_icon_bg_color,
        widget = wibox.container.background
    },
    {
        {
            format = '%a, %b %d ',
            font = main_font,
            widget = wibox.widget.textclock
        },
        fg = calendar_widget_fg_color,
        bg = calendar_widget_bg_color,
        widget = wibox.container.background
    },
    widget = wibox.layout.fixed.horizontal
}

caly = lain.widget.cal({
    attach_to = { theme.calendar_widget },
    notification_preset = {
        fg =  primary_widget_fg_color,
        bg =  primary_widget_bg_color,
        height = 400
    }
})

-- CLOCK WIDGET
clock_widget_icon = wibox.widget {
    markup = ' \u{f017} ',
    widget = wibox.widget.textbox
}
clock_widget_icon.font = theme.icon_font

theme.clock_widget = wibox.widget {
    {
        clock_widget_icon,
        fg = clock_widget_icon_fg_color,
        bg = clock_widget_icon_bg_color,
        widget = wibox.container.background
    },
    {
        {
            format = '%I:%M ',
            font = main_font,
            widget = wibox.widget.textclock
        },
        fg = clock_widget_fg_color,
        bg = clock_widget_bg_color,
        widget = wibox.container.background
    },
    widget = wibox.layout.fixed.horizontal
}

-- FS WIDGET
fs_widget_icon =  wibox.widget {
    markup = ' \u{f0a0} ',
    widget = wibox.widget.textbox
}
fs_widget_icon.font = theme.icon_font
fs_widget =  awful.widget.watch('bash /home/gurpinder/.config/awesome/themes/dracula/fs /dev/vda3', 5)
fs_widget.font = main_font
fs_widget.color = fs_widget_fg_color
theme.fs_widget = wibox.widget {
    {
        fs_widget_icon,
        fg = fs_widget_icon_fg_color,
        bg = fs_widget_icon_bg_color,
        widget = wibox.container.background
    },{
        fs_widget,
        fg = fs_widget_fg_color,
        bg = fs_widget_bg_color,
        widget = wibox.container.background
    },
    widget = wibox.layout.fixed.horizontal
}


-- Define the image to load
theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"



-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"



-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = 'Surfn'


return theme
